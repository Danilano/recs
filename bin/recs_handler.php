<?php

declare(strict_types=1);

use Danilano\Recs\RefactorIntegration\ConfigFoundException;
use Danilano\Recs\RefactorIntegration\ConfiguratorManager;
use Danilano\Recs\RefactorIntegration\PathsGetter;
use Danilano\Recs\RefactorIntegration\ProviderManager;
use Danilano\Recs\RefactorIntegration\RecsConfig;

/**
 * @var ProviderManager $pm
 * @var ConfiguratorManager $manager
 */
[$pm, $manager] = require __DIR__ . '/../src/autoload.php';

$basePath = VENDOR_PATH . '../';

if (file_exists($basePath . 'rector_config.php')) {
    require ($basePath . 'rector_config.php')($pm, $manager);
}

$recsConfigCb = require $basePath . 'recs.php';
//$recsConfigCb = require __DIR__ . '/../src/recs_example.php'; // EXAMPLE

$provider = $pm->resolveProviderByPrevClass(debug_backtrace()[1]['class'], [
    'paths' => (new PathsGetter($basePath))->get(), // TODO: сюда прокидывать параметры
]);

try {
    $recsConfigCb(
        new RecsConfig($manager, $provider),
    );
} catch (ConfigFoundException $exception) {
    return $exception->getResolvedCallable();
} catch (Throwable $exception) {
    dd($exception);
}
