<?php

namespace Danilano\Recs\RefactorIntegration;

use DirectoryIterator;
use SplFileInfo;

final readonly class PathsGetter
{
    public function __construct(
        private string $rootPath,
    ) {
    }

    /**
     * @return list<non-empty-string>
     */
    public function get(): array
    {
        $paths = [];
        foreach (new DirectoryIterator($this->rootPath) as $file) {
            /** @var SplFileInfo $file */

            $filename = $file->getFilename();

            if (
                $filename === 'vendor' ||
                str_starts_with($filename, '.')
            ) {
                continue;
            }

            if ($file->isDir()) {
                $filename .= '/';
            }

            $paths[] = $this->rootPath . $filename;
        }

        return $paths;
    }
}
