<?php

namespace Danilano\Recs\RefactorIntegration\Shared;


abstract readonly class AbstractConfigurator
{
    final private function __construct() { }

    abstract public static function supports(AbstractProvider $provider): bool;

    final public static function init(): static
    {
        return new static();
    }
}
