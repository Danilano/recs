<?php

namespace Danilano\Recs\RefactorIntegration\Shared;

abstract class AbstractProvider
{
    private function __construct(
        private array $extra,
    ) {
    }

    abstract public static function supports(string $prevCalledClass): bool;

    abstract public static function getBinPath(): string;

    abstract public static function getBinArguments(): array;

    public static function init(array $extra): static
    {
        return new static($extra);
    }

    public function setExtra(array $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }
}
