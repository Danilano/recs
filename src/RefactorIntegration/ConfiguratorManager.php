<?php

namespace Danilano\Recs\RefactorIntegration;

use Danilano\Recs\RefactorIntegration\Shared\AbstractConfigurator;
use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use DomainException;

class ConfiguratorManager
{
    /** @var list<class-string<AbstractConfigurator>> */
    private array $binded = [];

    public function bind(string $configuratorClass): self
    {
        if (!isset(class_parents($configuratorClass)[AbstractConfigurator::class])) {
            throw new DomainException('Configurator must implement Configurator class');
        }

        $this->binded[] = $configuratorClass;

        return $this;
    }

    /**
     * @return iterable<AbstractConfigurator>
     */
    public function getConfiguratorsForProvider(AbstractProvider $provider): iterable
    {
        foreach ($this->binded as $configuratorClass) {
            /** @var AbstractConfigurator $configuratorClass */

            if (!$configuratorClass::supports($provider)) {
                continue;
            }

            yield $configuratorClass::init();
        }
    }
}
