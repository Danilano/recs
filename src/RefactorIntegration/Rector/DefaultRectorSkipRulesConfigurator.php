<?php

namespace Danilano\Recs\RefactorIntegration\Rector;

use Danilano\Recs\RefactorIntegration\Shared\AbstractConfigurator;
use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use Rector\CodeQuality\Rector\Expression\TernaryFalseExpressionToIfRector;
use Rector\CodeQuality\Rector\Foreach_\UnusedForeachValueToArrayKeysRector;
use Rector\CodeQuality\Rector\FuncCall\CompactToVariablesRector;
use Rector\CodeQuality\Rector\Identical\FlipTypeControlToUseExclusiveTypeRector;
use Rector\CodingStyle\Rector\Assign\SplitDoubleAssignRector;
use Rector\CodingStyle\Rector\Catch_\CatchExceptionNameMatchingTypeRector;
use Rector\CodingStyle\Rector\ClassMethod\NewlineBeforeNewAssignSetRector;
use Rector\CodingStyle\Rector\Encapsed\EncapsedStringsToSprintfRector;
use Rector\CodingStyle\Rector\Encapsed\WrapEncapsedVariableInCurlyBracesRector;
use Rector\CodingStyle\Rector\If_\NullableCompareToNullRector;
use Rector\CodingStyle\Rector\Stmt\NewlineAfterStatementRector;
use Rector\CodingStyle\Rector\String_\SymplifyQuoteEscapeRector;
use Rector\Config\RectorConfig;
use Rector\Php80\Rector\Class_\ClassPropertyAssignToConstructorPromotionRector;
use Rector\PHPUnit\CodeQuality\Rector\Class_\PreferPHPUnitThisCallRector;
use Rector\Symfony\CodeQuality\Rector\Class_\EventListenerToEventSubscriberRector;

final readonly class DefaultRectorSkipRulesConfigurator extends AbstractConfigurator implements RectorConfiguratorInterface
{
    public static function supports(AbstractProvider $provider): bool
    {
        return $provider instanceof RectorProvider;
    }

    public function configure(array $extra, RectorConfig $rectorConfig): void
    {
        $rectorConfig->skip([
            ClassPropertyAssignToConstructorPromotionRector::class,
            FlipTypeControlToUseExclusiveTypeRector::class,
            WrapEncapsedVariableInCurlyBracesRector::class,
            CatchExceptionNameMatchingTypeRector::class,
            EventListenerToEventSubscriberRector::class,
            UnusedForeachValueToArrayKeysRector::class,
            TernaryFalseExpressionToIfRector::class,
            NewlineBeforeNewAssignSetRector::class,
            EncapsedStringsToSprintfRector::class,
            NullableCompareToNullRector::class,
            PreferPHPUnitThisCallRector::class,
            NewlineAfterStatementRector::class,
            SymplifyQuoteEscapeRector::class,
            CompactToVariablesRector::class,
            SplitDoubleAssignRector::class,
        ]);
    }
}
