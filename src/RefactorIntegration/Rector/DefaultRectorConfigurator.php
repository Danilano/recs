<?php

namespace Danilano\Recs\RefactorIntegration\Rector;

use Danilano\Recs\RefactorIntegration\Shared\AbstractConfigurator;
use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use Rector\Config\RectorConfig;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Php80\Rector\Class_\AnnotationToAttributeRector;
use Rector\Php80\ValueObject\AnnotationToAttribute;
use Rector\PHPUnit\Set\PHPUnitSetList;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;
use Symfony\Contracts\Service\Attribute\Required;

final readonly class DefaultRectorConfigurator extends AbstractConfigurator implements RectorConfiguratorInterface
{
    public static function supports(AbstractProvider $provider): bool
    {
        return $provider instanceof RectorProvider;
    }

    public function configure(array $extra, RectorConfig $rectorConfig): void
    {
        $rectorConfig->paths($extra['paths']);

        $rectorConfig->sets([
            SetList::DEAD_CODE,
            SetList::CODING_STYLE,
            SetList::CODE_QUALITY,
            SetList::PRIVATIZATION,
            SetList::TYPE_DECLARATION,

            SymfonySetList::SYMFONY_63,
            SymfonySetList::SYMFONY_CODE_QUALITY,
            SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,

            //LevelSetList::{'UP_TO_PHP_82'}, // TODO: Dynamic Constants from 8.3
            LevelSetList::UP_TO_PHP_81,

            DoctrineSetList::DOCTRINE_ORM_214,
            DoctrineSetList::DOCTRINE_CODE_QUALITY,
            DoctrineSetList::ANNOTATIONS_TO_ATTRIBUTES,

            PHPUnitSetList::PHPUNIT_CODE_QUALITY,
        ]);

        $rectorConfig->ruleWithConfiguration(AnnotationToAttributeRector::class, [
            new AnnotationToAttribute(Required::class),
        ]);
    }
}
