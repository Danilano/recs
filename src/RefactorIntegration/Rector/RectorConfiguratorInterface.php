<?php

namespace Danilano\Recs\RefactorIntegration\Rector;

use Rector\Config\RectorConfig;

interface RectorConfiguratorInterface
{
    public function configure(array $extra, RectorConfig $rectorConfig): void;
}
