<?php

namespace Danilano\Recs\RefactorIntegration\Rector;

use Danilano\Recs\RefactorIntegration\Shared\AbstractConfigurator;
use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use Rector\CodeQuality\Rector\If_\ExplicitBoolCompareRector;
use Rector\CodeQuality\Rector\Ternary\SwitchNegatedTernaryRector;
use Rector\CodingStyle\Rector\FuncCall\ArraySpreadInsteadOfArrayMergeRector;
use Rector\Config\RectorConfig;
use Rector\Doctrine\CodeQuality\Rector\Property\OrderByKeyToClassConstRector;
use Rector\Php73\Rector\FuncCall\JsonThrowOnErrorRector;
use Rector\Php74\Rector\Closure\ClosureToArrowFunctionRector;
use Rector\Php80\Rector\Class_\StringableForToStringRector;
use Rector\Php81\Rector\Class_\MyCLabsClassToEnumRector;
use Rector\Php81\Rector\MethodCall\MyCLabsMethodCallToEnumConstRector;
use Rector\Strict\Rector\Empty_\DisallowedEmptyRuleFixerRector;
use Rector\Transform\Rector\Attribute\AttributeKeyToClassConstFetchRector;

final readonly class DefaultRectorSkipRiskyRulesConfigurator extends AbstractConfigurator implements RectorConfiguratorInterface
{
    public static function supports(AbstractProvider $provider): bool
    {
        return $provider instanceof RectorProvider;
    }

    public function configure(array $extra, RectorConfig $rectorConfig): void
    {
        $rectorConfig->skip([
            ArraySpreadInsteadOfArrayMergeRector::class,
            AttributeKeyToClassConstFetchRector::class, // Чёт он сходит с ума
            MyCLabsMethodCallToEnumConstRector::class,
            MyCLabsClassToEnumRector::class,
            DisallowedEmptyRuleFixerRector::class,
            ClosureToArrowFunctionRector::class,
            OrderByKeyToClassConstRector::class,
            StringableForToStringRector::class,
            SwitchNegatedTernaryRector::class,
            ExplicitBoolCompareRector::class,
            JsonThrowOnErrorRector::class,
        ]);
    }
}
