<?php

namespace Danilano\Recs\RefactorIntegration\Rector;

use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use Rector\Config\RectorConfig;

class RectorProvider extends AbstractProvider
{
    public static function supports(string $prevCalledClass): bool
    {
        return $prevCalledClass === RectorConfig::class;
    }

    public static function getBinPath(): string
    {
        return 'bin/rector';
    }

    public static function getBinArguments(): array
    {
        return ['--config=bin/recs_handler.php'];
    }

    /**
     * @param iterable<RectorConfiguratorInterface> $configurators
     */
    public function configure(iterable $configurators, callable $callable): callable
    {
        return function (RectorConfig $rectorConfig) use ($configurators, $callable): void {
            foreach ($configurators as $configurator) {
                $configurator->configure(
                    $this->getExtra(),
                    $rectorConfig,
                );
            }

            $callable($rectorConfig);
        };
    }
}
