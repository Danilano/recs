<?php

namespace Danilano\Recs\RefactorIntegration\Ecs;

use Symplify\EasyCodingStandard\Config\ECSConfig;

interface EcsConfiguratorInterface
{
    public function configure(array $extra, ECSConfig $ecsConfig): void;
}
