<?php

namespace Danilano\Recs\RefactorIntegration\Ecs;

use Danilano\Recs\RefactorIntegration\Shared\AbstractConfigurator;
use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;
use PhpCsFixer\Fixer\ArrayNotation\NoMultilineWhitespaceAroundDoubleArrowFixer;
use PhpCsFixer\Fixer\AttributeNotation\AttributeEmptyParenthesesFixer;
use PhpCsFixer\Fixer\Basic\OctalNotationFixer;
use PhpCsFixer\Fixer\ClassNotation\ClassAttributesSeparationFixer;
use PhpCsFixer\Fixer\ClassNotation\ClassDefinitionFixer;
use PhpCsFixer\Fixer\ClassNotation\SelfStaticAccessorFixer;
use PhpCsFixer\Fixer\Comment\NoEmptyCommentFixer;
use PhpCsFixer\Fixer\Comment\SingleLineCommentSpacingFixer;
use PhpCsFixer\Fixer\Comment\SingleLineCommentStyleFixer;
use PhpCsFixer\Fixer\ControlStructure\TrailingCommaInMultilineFixer;
use PhpCsFixer\Fixer\ControlStructure\YodaStyleFixer;
use PhpCsFixer\Fixer\FunctionNotation\NullableTypeDeclarationForDefaultNullValueFixer;
use PhpCsFixer\Fixer\FunctionNotation\SingleLineThrowFixer;
use PhpCsFixer\Fixer\Import\GlobalNamespaceImportFixer;
use PhpCsFixer\Fixer\Import\OrderedImportsFixer;
use PhpCsFixer\Fixer\LanguageConstruct\SingleSpaceAfterConstructFixer;
use PhpCsFixer\Fixer\LanguageConstruct\SingleSpaceAroundConstructFixer;
use PhpCsFixer\Fixer\Operator\BinaryOperatorSpacesFixer;
use PhpCsFixer\Fixer\Operator\ConcatSpaceFixer;
use PhpCsFixer\Fixer\Operator\IncrementStyleFixer;
use PhpCsFixer\Fixer\Operator\NoSpaceAroundDoubleColonFixer;
use PhpCsFixer\Fixer\Operator\NotOperatorWithSuccessorSpaceFixer;
use PhpCsFixer\Fixer\Operator\OperatorLinebreakFixer;
use PhpCsFixer\Fixer\Phpdoc\NoBlankLinesAfterPhpdocFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocAlignFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocSeparationFixer;
use PhpCsFixer\Fixer\Semicolon\MultilineWhitespaceBeforeSemicolonsFixer;
use PhpCsFixer\Fixer\Strict\DeclareStrictTypesFixer;
use PhpCsFixer\Fixer\Whitespace\HeredocIndentationFixer;
use PhpCsFixer\Fixer\Whitespace\MethodChainingIndentationFixer;
use PhpCsFixer\Fixer\Whitespace\NoExtraBlankLinesFixer;
use PhpCsFixer\Fixer\Whitespace\StatementIndentationFixer;
use Symplify\CodingStandard\Fixer\Commenting\ParamReturnAndVarTagMalformsFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

final readonly class DefaultEcsConfigurator extends AbstractConfigurator implements EcsConfiguratorInterface
{
    public static function supports(AbstractProvider $provider): bool
    {
        return $provider instanceof EcsProvider;
    }

    public function configure(array $extra, ECSConfig $ecsConfig): void
    {
        $ecsConfig->paths($extra['paths']);

        $phpVersion = explode('.', PHP_VERSION);
        $ecsConfig->dynamicSets([
            sprintf(
                '@PHP%s%sMigration',
                $phpVersion[0],
                $phpVersion[1],
            ),
            '@Symfony',
        ]);

        $ecsConfig->sets([
            SetList::CLEAN_CODE,
            SetList::LARAVEL,
            SetList::PSR_12,
        ]);

        $ecsConfig->skip([
            NullableTypeDeclarationForDefaultNullValueFixer::class,
            NoMultilineWhitespaceAroundDoubleArrowFixer::class,
            NotOperatorWithSuccessorSpaceFixer::class,
            ParamReturnAndVarTagMalformsFixer::class,
            SingleSpaceAroundConstructFixer::class,
            MethodChainingIndentationFixer::class,
            SingleSpaceAfterConstructFixer::class,
            ClassAttributesSeparationFixer::class,
            NoSpaceAroundDoubleColonFixer::class,
            SingleLineCommentSpacingFixer::class,
            NoBlankLinesAfterPhpdocFixer::class,
            SingleLineCommentStyleFixer::class,
            GlobalNamespaceImportFixer::class,
            BinaryOperatorSpacesFixer::class,
            StatementIndentationFixer::class,
            HeredocIndentationFixer::class,
            SelfStaticAccessorFixer::class,
            NoExtraBlankLinesFixer::class,
            PhpdocSeparationFixer::class,
            SingleLineThrowFixer::class,
            ClassDefinitionFixer::class,
            NoEmptyCommentFixer::class,
            IncrementStyleFixer::class,
            OctalNotationFixer::class,
            ConcatSpaceFixer::class,
            PhpdocAlignFixer::class,
        ]);

        $ecsConfig->lineEnding("\n");

        $ecsConfig->ruleWithConfiguration(MultilineWhitespaceBeforeSemicolonsFixer::class, [
            'strategy' => 'new_line_for_chained_calls',
        ]);
        $ecsConfig->ruleWithConfiguration(AttributeEmptyParenthesesFixer::class, [
        ]);
        $ecsConfig->ruleWithConfiguration(TrailingCommaInMultilineFixer::class, [
            'elements' => ['arguments', 'arrays', 'match', 'parameters'],
        ]);
        $ecsConfig->ruleWithConfiguration(OperatorLinebreakFixer::class, [
            'position' => 'end',
            'only_booleans' => true,
        ]);
        $ecsConfig->ruleWithConfiguration(OrderedImportsFixer::class, [
            'imports_order' => [
                'class',
                'const',
                'function',
            ],
        ]);
        $ecsConfig->ruleWithConfiguration(ArraySyntaxFixer::class, [
            'syntax' => 'short',
        ]);
        $ecsConfig->ruleWithConfiguration(YodaStyleFixer::class, [
            'equal' => false,
            'identical' => false,
            'less_and_greater' => false,
        ]);
        $ecsConfig->rule(DeclareStrictTypesFixer::class);
    }
}
