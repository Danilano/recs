<?php

namespace Danilano\Recs\RefactorIntegration\Ecs;

use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use Symplify\EasyCodingStandard\Config\ECSConfig;

class EcsProvider extends AbstractProvider
{
    public static function supports(string $prevCalledClass): bool
    {
        return str_contains($prevCalledClass, 'Symplify\EasyCodingStandard');
    }

    public static function getBinPath(): string
    {
        return 'bin/ecs';
    }

    public static function getBinArguments(): array
    {
        return ['--config=bin/recs_handler.php', '--fix'];
    }

    /**
     * @param iterable<EcsConfiguratorInterface> $configurators
     */
    public function configure(iterable $configurators, callable $callable): callable
    {
        return function (ECSConfig $ecsConfig) use ($configurators, $callable): void {
            foreach ($configurators as $configurator) {
                $configurator->configure(
                    $this->getExtra(),
                    $ecsConfig,
                );
            }

            $callable($ecsConfig);
        };
    }
}
