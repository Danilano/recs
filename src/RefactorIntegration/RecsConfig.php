<?php

namespace Danilano\Recs\RefactorIntegration;

use BadMethodCallException;
use Danilano\Recs\RefactorIntegration\Shared\AbstractConfigurator;
use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use ReflectionClass;

/**
 * @method callable configureEcs(callable $ecsConfig)
 * @method callable configureRector(callable $rectorConfig)
 */
final readonly class RecsConfig
{
    public function __construct(
        private ConfiguratorManager $configuratorManager,
        private AbstractProvider $provider,
    ) {
    }

    /**
     * @param class-string<AbstractConfigurator> $configuratorClass
     */
    public function addConfigurator(string $configuratorClass): self
    {
        $this->configuratorManager->bind($configuratorClass);

        return $this;
    }

    public function getExtra(): array
    {
        return $this->provider->getExtra();
    }

    public function setExtra(array $extra): self
    {
        $this->provider->setExtra($extra);

        return $this;
    }

    public function __call(string $name, array $arguments): null
    {
        if (!str_starts_with($name, 'configure')) {
            throw new BadMethodCallException('Configurator not found');
        }

        $name = str_replace('configure', '', $name);

        if ("{$name}Provider" !== (new ReflectionClass($this->provider))->getShortName()) {
            return null;
        }

        ConfigFoundException::throw(
            $this->provider->configure(
                $this->configuratorManager->getConfiguratorsForProvider($this->provider),
                ...$arguments,
            ),
        );
    }
}
