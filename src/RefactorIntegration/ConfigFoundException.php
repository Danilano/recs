<?php

namespace Danilano\Recs\RefactorIntegration;

use Exception;

class ConfigFoundException extends Exception
{
    /** @var callable $resolvedCallable */
    private $resolvedCallable;

    final private function __construct(callable $resolvedCallable)
    {
        $this->resolvedCallable = $resolvedCallable;

        parent::__construct();
    }

    /**
     * @throws ConfigFoundException
     */
    public static function throw(callable $resolvedCallable): void
    {
        throw new self($resolvedCallable);
    }

    public function getResolvedCallable(): callable
    {
        return $this->resolvedCallable;
    }
}
