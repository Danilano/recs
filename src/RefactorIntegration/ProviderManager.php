<?php

namespace Danilano\Recs\RefactorIntegration;

use Danilano\Recs\RefactorIntegration\Shared\AbstractProvider;
use DomainException;

class ProviderManager
{
    /** @var list<class-string<AbstractProvider>> */
    private array $binded = [];

    public function bind(string $providerClass): self
    {
        if (!isset(class_parents($providerClass)[AbstractProvider::class])) {
            throw new DomainException('Configurator must implement Configurator class');
        }

        $this->binded[] = $providerClass;

        return $this;
    }

    /**
     * @return iterable<non-empty-string, list<string>>
     */
    public function getProvidersMeta(): iterable
    {
        foreach ($this->binded as $providerClass) {
            /** @var AbstractProvider $providerClass */

            yield $providerClass::getBinPath() => $providerClass::getBinArguments();
        }
    }

    public function resolveProviderByPrevClass(string $prevCalledClass, array $extra): AbstractProvider
    {
        foreach ($this->binded as $providerClass) {
            /** @var AbstractProvider $providerClass */

            if (!$providerClass::supports($prevCalledClass)) {
                continue;
            }

            return $providerClass::init($extra);
        }

        throw new DomainException('Provider not found for ' . $prevCalledClass);
    }
}
