<?php

use Danilano\Recs\RefactorIntegration\ConfiguratorManager;
use Danilano\Recs\RefactorIntegration\Ecs\DefaultEcsConfigurator;
use Danilano\Recs\RefactorIntegration\Ecs\EcsProvider;
use Danilano\Recs\RefactorIntegration\ProviderManager;
use Danilano\Recs\RefactorIntegration\Rector\DefaultRectorConfigurator;
use Danilano\Recs\RefactorIntegration\Rector\RectorProvider;

if (!defined('VENDOR_PATH')) {
    $foundVendorPath = null;
    foreach (
        [
            __DIR__ . '/../vendor/',
            __DIR__ . '/../../../',
        ] as $vendorPath
    ) {
        if (file_exists($vendorPath . 'autoload.php')) {
            $foundVendorPath = $vendorPath;

            break;
        }
    }

    if (!$foundVendorPath) {
        echo "Autoload file not found; try 'composer dump-autoload' first." . PHP_EOL;
        exit(1);
    }

    define('VENDOR_PATH', $foundVendorPath);

    require VENDOR_PATH . 'autoload.php';
}

$pm = new ProviderManager();
$pm
    ->bind(EcsProvider::class)
    ->bind(RectorProvider::class)
;

$manager = new ConfiguratorManager();
$manager->bind(DefaultEcsConfigurator::class);
$manager->bind(DefaultRectorConfigurator::class);

return [$pm, $manager];
