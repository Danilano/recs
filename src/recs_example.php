<?php

use Danilano\Recs\RefactorIntegration\Ecs\DefaultEcsConfigurator;
use Danilano\Recs\RefactorIntegration\RecsConfig;
use Danilano\Recs\RefactorIntegration\Rector\DefaultRectorConfigurator;
use Danilano\Recs\RefactorIntegration\Rector\DefaultRectorSkipRulesConfigurator;
use Rector\Caching\ValueObject\Storage\FileCacheStorage;
use Rector\Config\RectorConfig;
use Symplify\EasyCodingStandard\Config\ECSConfig;

return static function (RecsConfig $recsConfig): void {
    $recsConfig
        ->addConfigurator(DefaultEcsConfigurator::class)
        ->addConfigurator(DefaultRectorConfigurator::class)
        ->addConfigurator(DefaultRectorSkipRulesConfigurator::class)
    ;

    $extra = $recsConfig->getExtra();
    $extra['paths'] = array_filter(
        $extra['paths'],
        static fn (string $path): bool => !str_ends_with($path, '/var/'),
    );

    $recsConfig->setExtra($extra);

    $recsConfig
        ->configureEcs(
            static function (ECSConfig $ecsConfig): void {
                $ecsConfig->parallel();
                $ecsConfig->cacheDirectory(__DIR__ . '/var/cache/ecs');

                $ecsConfig->skip([
                    __DIR__ . '/var',
                ]);
            },
        )
    ;

    $recsConfig
        ->configureRector(
            static function (RectorConfig $rectorConfig): void {
                $rectorConfig->parallel(seconds: 240, maxNumberOfProcess: 4);
                $rectorConfig->symfonyContainerXml(__DIR__ . '/var/cache/dev/App_KernelDevDebugContainer.xml');

                $rectorConfig->cacheClass(FileCacheStorage::class);
                $rectorConfig->cacheDirectory(__DIR__ . '/var/cache/rector');

                $rectorConfig->skip([
                    __DIR__ . '/var',
                ]);
            },
        )
    ;
};
