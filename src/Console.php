<?php

namespace Danilano\Recs;

use Danilano\Recs\RefactorIntegration\ProviderManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

#[AsCommand(
    name: 'danilano:recs',
    description: 'Recs'
)]
final class Console extends Command
{
    public function __construct(
        private readonly ProviderManager $providerManager,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption(
            name: 'init',
            description: 'Скопировать демо в корень проекта',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getOption('init')) {
            copy(__DIR__ . '/recs_example.php', VENDOR_PATH . '../recs.php');

            $output->writeln('Был создан пример конфигурации');

            return Command::SUCCESS;
        }

        if (!file_exists(VENDOR_PATH . '../recs.php')) {
            $output->writeln('Настройки не инициализированы, необходимо сначала настроить конфигурацию');

            return Command::FAILURE;
        }

        foreach ($this->providerManager->getProvidersMeta() as $binPath => $arguments) {
            $process = new Process(
                command: ['php', VENDOR_PATH . $binPath, ...$arguments],
                cwd: dirname(__DIR__),
                timeout: 240,
            );
            $process->setTty(true); // Цвет

            $process->run(function (string $type, string $buffer): void {
                echo $buffer;
            });

            break;
        }

        return Command::SUCCESS;
    }
}
